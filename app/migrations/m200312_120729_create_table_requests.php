<?php

use yii\db\Migration;

/**
 * Class m200312_120729_create_table_requests
 */
class m200312_120729_create_table_requests extends Migration
{
    /**
     * @var string $userAgentTable Наименование таблицы, куда будет сохраняться информация об User-Agent
     */
    private  $userAgentTable = '{{%user_agent}}';

    /**
     * @var string $requestTable Наименование таблицы, куда будет сохраняться информация о запросах к серверу NGINX
     */
    private  $requestTable = '{{%request}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->userAgentTable,[
            'id'              => $this->primaryKey(),
            'userAgent'       => $this->string()->notNull()->comment('User-Agent'),
            'operationSystem' => $this->string()->notNull()->comment('Операционная система'),
            'architecture'    => $this->string()->notNull()->comment('Архитектура'),
            'browser'         => $this->string()->notNull()->comment('Браузер')
        ]);

        $this->createTable($this->requestTable,[
            'id'              => $this->primaryKey(),
            'host'            => $this->string()->notNull()->comment('Хост'),
            'ip'              => $this->string(40)->notNull()->comment('IP - адрес'),
            'date'            => $this->dateTime()->notNull()->comment('Дата/время запроса'),
            'url'             => $this->string()->notNull()->comment('URL'),
            'userAgentId'     => $this->integer()->notNull()->comment('User-Agent'),
            'operationSystem' => $this->string()->notNull()->comment('Операционная система'),
            'architecture'    => $this->string()->notNull()->comment('Архитектура'),
            'browser'         => $this->string()->notNull()->comment('Браузер')
        ]);

        $this->addForeignKey('fk_request_userAgentId', $this->requestTable, 'userAgentId', $this->userAgentTable, 'id');
        $this->createIndex('index_request_operationSystem',$this->requestTable,'operationSystem');
        $this->createIndex('index_request_architecture',$this->requestTable,'architecture');
        $this->createIndex('index_request_architecture_operationSystem',$this->requestTable,'operationSystem,architecture');
        $this->createIndex('index_request_url',$this->requestTable,'url');
        $this->createIndex('index_request_browser',$this->requestTable,'browser');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('index_request_browser',$this->requestTable);
        $this->dropIndex('index_request_url',$this->requestTable);
        $this->dropIndex('index_request_architecture_operationSystem',$this->requestTable);
        $this->dropIndex('index_request_architecture',$this->requestTable);
        $this->dropIndex('index_request_operationSystem',$this->requestTable);
        $this->dropForeignKey('fk_request_userAgentId',$this->requestTable);
        $this->dropTable($this->requestTable);
        $this->dropTable($this->userAgentTable);
    }


}
