<?php

namespace app\controllers;

use Yii;
use app\models\Statistics;
use yii\web\Controller;
use yii\filters\VerbFilter;
use DomainException;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @return bool|string
     * @throws \yii\db\Exception
     */
    public function actionIndex()
    {
        try {
            $searchModel = new Statistics();
            $searchModel->load(Yii::$app->request->get());
            if (!$searchModel->validate()) {
                return $this->render('_search', [
                    'model'   => $searchModel,
                ]);
            };

            return $this->render('index', [
                'searchModel'   => $searchModel,
                'dataProvider1' => $searchModel->getChart1Data(),
                'dataProvider2' => $searchModel->getChart2Data(),
                'dataProvider3' => $searchModel->getTableData()
            ]);
        } catch (DomainException $exception) {
            return  $this->render('error',[
                'name' => 500,
                'message' => $exception->getMessage()
            ]);
        }


    }

}
