<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Statistics;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model Statistics */
/* @var $form ActiveForm */
?>

<div>

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?echo $form->errorSummary($model);?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'dateFrom')->widget(DatePicker::class, [
                'dateFormat' => 'dd.MM.yyyy',
                'options' => ['class' => 'form-control'],
            ]) ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'dateTo')->widget(DatePicker::class, [
                'dateFormat' => 'dd.MM.yyyy',
                'options' => ['class' => 'form-control'],
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'operationSystem')->dropDownList(ArrayHelper::merge([''=>''],$model->getOperationSystems())) ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'architecture')->dropDownList(ArrayHelper::merge([''=>''],$model->getArchitecture())) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary','id' => 'search']) ?>
        <?= Html::a('Сбросить фильтры','index', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
