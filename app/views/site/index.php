<?php
use sjaakp\gcharts\ColumnChart;
use yii\data\ActiveDataProvider;
use app\models\request\RequestSearch;
use app\models\Statistics;

/**
 * @var $dataProvider1 ActiveDataProvider
 * @var $dataProvider2 ActiveDataProvider
 * @var $dataProvider3 ActiveDataProvider
 * @var $model RequestSearch
 * @var $searchModel Statistics
 */
?>

<?php echo $this->render('_search', ['model' => $searchModel]); ?>

<div class="row">
    <?= \yii\grid\GridView::widget([
        'tableOptions' => ['class' => 'table table-hover'],
        'dataProvider' => $dataProvider3,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute'=>'normalizeDate',
                'label'=>'Дата',
                'contentOptions' => ['style' => 'min-width:100px; word-wrap: break-word;'],
            ],
            [
                'attribute'=>'url',
                'label'=>'Url',
                'contentOptions' => ['style' => 'max-width:500px; word-wrap: break-word;'],
            ],
            [
                'attribute'=>'browser',
                'label'=>'Браузер',
            ],
            [
                'attribute'=>'count',
                'label'=>'Запросы',
            ]
        ],
    ]); ?>
</div>

<div class="row">
        <?= ColumnChart::widget([
            'height' => '400px',
            'dataProvider' => $dataProvider1,
            'columns' => [
                [
                    'value' => function($model) {
                        return "{$model->date}\n [ $model->count ]";
                    },
                    'type' => 'string',
                ],
                [
                    'label' => 'Количество запросов',
                    'value' => function($model) {
                        return "$model->count";
                    }
                ],
            ],
            'options' => [
                'title' => 'Количество запросов в разбивке по датам',
            ],
        ]) ?>
</div>

<div class="row">
        <?= ColumnChart::widget([
            'height' => '400px',
            'dataProvider' => $dataProvider2,
            'columns' => [
                [
                    'value' => function($model) {
                        return "{$model->browser}\n % [ $model->count ]";
                    },
                    'type' => 'string',
                ],
                [
                    'label' => 'Процент запросов',
                    'value' => function($model) {
                        return "$model->count";
                    }
                ],
                [               // third column: tooltip
                    'value' => function($model) {
                        return "{$model->browser}\n % $model->count";
                    },
                    'type' => 'string',
                    'role' => 'tooltip',
                ],
            ],
            'options' => [
                'title' => 'доля (% от числа запросов) для трех самых популярных браузеров',
                'colors' => ['green'],
            ],
        ]) ?>
</div>





