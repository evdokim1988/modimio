<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\request\Request;
use app\models\userAgent\UserAgent;
use yii\console\Controller;
use DateTime;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ParserController extends Controller
{

    public function actionIndex()
    {
        //        $host = "http://default.loc/";
        //        $filePath = Url::to('@app/web/assets/nginx/access.log');

        $host = $this->prompt('Введите валидный url хоста, логи котрого мы будем парсить. Пример: http://default.loc/' . PHP_EOL, ['required' => true]);

        if (filter_var($host, FILTER_VALIDATE_URL)) {
            echo("$host is a valid URL. Continue ... ") . PHP_EOL;
        } else {
            exit("$host is not a valid URL" . PHP_EOL);
        }

        $filePath = $this->prompt('Введите абсолютный путь до файла логов, который мы будем парсить' . PHP_EOL, ['required' => true]);

        if (file_exists($filePath)) {
            echo("$filePath is a valid file path. Continue ... ") . PHP_EOL;
        } else {
            exit("$filePath is not a valid file path" . PHP_EOL);
        }

        $text = fopen($filePath, "r");
        $array = null;
        if ($text) {
            while (($buffer = fgets($text)) !== false) {
                $pattern = "/(\S+) (\S+) (\S+) \[([^:]+):(\d+:\d+:\d+) ([^\]]+)\] \"(\S+) (.*?) (\S+)\" (\S+) (\S+) (\".*?\") (\".*?\")/";

                preg_match ($pattern, $buffer, $result);

                $request = [];
                $request['ip']       = $result[1];
                $request['identity'] = $result[2];
                $request['user']     = $result[3];
                $request['date']     = $result[4];
                $request['time']     = $result[5];
                $request['timezone'] = $result[6];
                $request['method']   = $result[7];
                $request['path']     = $result[8];
                $request['protocol'] = $result[9];
                $request['status']   = $result[10];
                $request['bytes']    = $result[11];
                $request['referer']  = $result[12];
                $request['agent']    = $result[13];

                $pattern = "/\".*? \((.*?)\) (.*?)\"$/";
                preg_match ($pattern, $result[13], $matches);
                $request['operationSystem'] = $matches[1];
                $request['browser'] = $matches[2];

                $pattern = "/.*?64.*?/";
                $request['architecture'] = preg_match ($pattern, $matches[1]) === false ? '32' : '64';

                $array[] = $request;

                $userAgent = UserAgent::find()->where(['userAgent' => $request['agent']])->one();
                if (!$userAgent) {
                    $userAgent = new UserAgent();
                    $userAgent->userAgent = $request['agent'];
                    $userAgent->operationSystem = $request['operationSystem'];
                    $userAgent->architecture = $request['architecture'];
                    $userAgent->browser = $request['browser'];
                    $userAgent->save();
                }

                $date = str_replace('/','-',$request['date']);
                $date = new DateTime($date. ' ' . $request['time']);
                $dateTime = $date->format('Y-m-d H:i:s');

                $exist = Request::find()->where(['and',
                    ['host' => $host],
                    ['url' =>  $request['path']],
                    ['date' => $dateTime],
                    ['userAgentId' => $userAgent->id]
                ])->exists();

                if (!$exist) {
                    $nginxRequest = new Request();
                    $nginxRequest->host = $host;
                    $nginxRequest->ip = $request['ip'];
                    $nginxRequest->date = $dateTime;
                    $nginxRequest->url = $request['path'];
                    $nginxRequest->userAgentId = $userAgent->id;
                    $nginxRequest->operationSystem = $request['operationSystem'];
                    $nginxRequest->architecture = $request['architecture'];
                    $nginxRequest->browser = $request['browser'];
                    $nginxRequest->save();

                }
            }
        }
        fclose($text);

        print_r($array);
    }
}
