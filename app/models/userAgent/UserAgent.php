<?php

namespace app\models\userAgent;

use app\models\request\Request;
use app\models\request\RequestQuery;
use yii\db\ActiveRecord;
use yii\db\ActiveQuery;
use Yii;

/**
 * This is the model class for table "{{%user_agent}}".
 *
 * @property int $id
 * @property string $userAgent User-Agent
 * @property string $operationSystem Операционная система
 * @property string $architecture Архитектура
 * @property string $browser Браузер
 *
 * @property Request[] $requests
 */
class UserAgent extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user_agent}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['operationSystem', 'architecture', 'browser'], 'required'],
            [['operationSystem', 'architecture', 'browser'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'              => Yii::t('app', 'ID'),
            'userAgent'       => Yii::t('app', 'User-Agent'),
            'operationSystem' => Yii::t('app', 'Операционная система'),
            'architecture'    => Yii::t('app', 'Архитектура'),
            'browser'         => Yii::t('app', 'Браузер'),
        ];
    }

    /**
     * Gets query for [[Requests]].
     *
     * @return ActiveQuery|RequestQuery
     */
    public function getRequests()
    {
        return $this->hasMany(Request::class, ['userAgentId' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return UserAgentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserAgentQuery(get_called_class());
    }
}
