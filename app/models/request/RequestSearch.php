<?php

namespace app\models\request;

use app\models\userAgent\UserAgent;

/**
 * This is the model class for table "{{%request}}".
 *
 * @property int $id
 * @property string $host Хост
 * @property string $ip IP - адрес
 * @property string $date Дата/время запроса
 * @property string $url URL
 * @property int $userAgentId User-Agent
 *
 * @property UserAgent $userAgent
 */
class RequestSearch extends Request
{
    /**
     * @var $count integer Служебная переменная для посчета количества различных записей
     */
    public $count;

}
