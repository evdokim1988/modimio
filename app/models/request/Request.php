<?php

namespace app\models\request;

use app\models\userAgent\UserAgent;
use app\models\userAgent\UserAgentQuery;
use yii\db\ActiveRecord;
use yii\db\ActiveQuery;
use Yii;

/**
 * This is the model class for table "{{%request}}".
 *
 * @property int $id
 * @property string $host Хост
 * @property string $ip IP - адрес
 * @property string $date Дата/время запроса
 * @property string $url URL
 * @property int $userAgentId User-Agent
 * @property string $operationSystem Операционная система
 * @property string $architecture Архитектура
 * @property string $browser Браузер
 *
 * @property UserAgent $userAgent
 */
class Request extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%request}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ip', 'date', 'url', 'userAgentId','operationSystem','architecture','browser'], 'required'],
            [['date'], 'safe'],
            [['userAgentId'], 'integer'],
            [['ip'], 'string', 'max' => 40],
            [['url','operationSystem','architecture','browser'], 'string', 'max' => 255],
            [['userAgentId'], 'exist', 'skipOnError' => true, 'targetClass' => UserAgent::class, 'targetAttribute' => ['userAgentId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'host' => Yii::t('app', 'Хост'),
            'ip' => Yii::t('app', 'IP - адрес'),
            'date' => Yii::t('app', 'Дата/время запроса'),
            'url' => Yii::t('app', 'URL'),
            'userAgentId' => Yii::t('app', 'User-Agent'),
            'operationSystem' => Yii::t('app', 'Операционная система'),
            'architecture'    => Yii::t('app', 'Архитектура'),
            'browser'         => Yii::t('app', 'Браузер'),
        ];
    }

    /**
     * Gets query for [[UserAgent]].
     *
     * @return ActiveQuery|UserAgentQuery
     */
    public function getUserAgent()
    {
        return $this->hasOne(UserAgent::class, ['id' => 'userAgentId']);
    }

    /**
     * {@inheritdoc}
     * @return RequestQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RequestQuery(get_called_class());
    }
}
