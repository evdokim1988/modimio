<?php

namespace app\models;

use app\models\request\Request;
use app\models\request\RequestSearch;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use Yii;
use yii\db\Query;
use Exception;
use DateTime;
use DomainException;

/**
 * @property string $dateFrom Дата начала отчета
 * @property string $dateTo Дата окончания отчета
 * @property string $operationSystem Операционная система
 * @property string $architecture Архитектура
 */

class Statistics extends Model
{
    /**
     * @var $_dateFrom string Дата начала отчета
     */
    private $_dateFrom;

    /**
     * @var $_dateTo string Дата окончания отчета
     */
    private $_dateTo;

    /**
     * @var $operationSystem string Операционная система
     */
    public $operationSystem;

    /**
     * @var $architecture string Архитектура
     */
    public $architecture;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['dateFrom','dateTo'], 'required'],
            [['operationSystem','architecture'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'dateFrom' => Yii::t('app', 'Дата начала'),
            'dateTo' => Yii::t('app', 'Дата окончания'),
            'operationSystem' => Yii::t('app', 'Операционная система'),
            'architecture' => Yii::t('app', 'Разрядность'),
        ];
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function beforeValidate()
    {
        $this->validateDates();
        return parent::beforeValidate();
    }

    /**
     * Валидация периода дат
     *
     * @throws Exception
     */
    public function validateDates()
    {
        $dateError = false;
        $dateFrom = $this->getDateFrom();
        $dateTo = $this->getDateTo();

        try{
            if (mb_strlen($dateFrom) === 6) throw new Exception();
            $date1 = new DateTime($dateFrom);
        }catch (Exception $exception) {
            $this->addError('dateFrom', Yii::t('app', 'Ошибка в формате даты') . ' ' . $dateFrom);
            $this->_dateFrom = null;
            $dateError = true;
        }

        try{
            if (mb_strlen($dateTo) === 6) throw new Exception();
            $date2 = new DateTime($dateTo);
        }catch (Exception $exception) {
            $this->addError('dateFrom', Yii::t('app', 'Ошибка в формате даты') . ' ' . $dateTo);
            $this->_dateTo = null;
            $dateError = true;
        }

        if ($dateError) return;

        $interval = date_diff($date1,$date2);

        if (abs($interval->y) > 0) {
             $this->addError('dateFrom', Yii::t('app', 'Период дат не может быть более 1 ого года'));
             $this->addError('dateTo', Yii::t('app', 'Период дат не может быть более 1 ого года'));
        }
    }


    /**
     * @return ActiveDataProvider
     * @throws \Exception
     */
    public function getChart1Data()
    {
        $date1 = $this->normalizeDate($this->getDateFrom());
        $date2 = $this->normalizeDate($this->getDateTo());

        $query = RequestSearch::find()->select('DATE_FORMAT(`date`, "%m-%d-%Y") as `date`, count(id) as `count`')->groupBy(['DATE_FORMAT(`date`, "%m-%d-%Y")']);;

        $query->andWhere(['and',['>=','date', $date1],['<=','date', $date2]]);

        $query->andFilterWhere(['and',
            ['operationSystem' => $this->operationSystem],
            ['architecture' => $this->architecture],
        ]);

        return $dataProvider1 = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false
        ]);
    }

    /**
     * @return ActiveDataProvider
     * @throws \Exception
     */
    public function getChart2Data()
    {
        $date1 = $this->normalizeDate($this->getDateFrom());
        $date2 = $this->normalizeDate($this->getDateTo());

        $count = RequestSearch::find()->select('count(id) as `sum`')
            ->andWhere(['and',['>=','date', $date1],['<=','date', $date2]])
            ->andFilterWhere(['and',
                ['operationSystem' => $this->operationSystem],
                ['architecture' => $this->architecture],
            ])->scalar();

        $query = RequestSearch::find();
        $query->select('browser, ifnull(count(id)/('. $count .'),0)*100 as `count`')
            ->groupBy('browser')->orderBy('count(id) desc')->limit(3);

        $query->andFilterWhere(['and',
            ['operationSystem' => $this->operationSystem],
            ['architecture' => $this->architecture],
        ]);

        $date1 = $this->normalizeDate($this->getDateFrom());
        $date2 = $this->normalizeDate($this->getDateTo());

        $query->andWhere(['and',['>=','date', $date1],['<=','date', $date2]]);

        return $dataProvider2 = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false
        ]);
    }

    /**
     * @return ArrayDataProvider
     * @throws \yii\db\Exception
     */
    public function getTableData()
    {
        $date1 = $this->normalizeDate($this->getDateFrom());
        $date2 = $this->normalizeDate($this->getDateTo());

        $table = Request::tableName();
        $query1 = (new Query())
            ->select(['DATE_FORMAT(date, "%m-%d-%Y") as `normalizeDate`', 'count(id) as `count`', 'browser'])
            ->from($table)
            ->where(['and',['>=','date', $date1],['<=','date', $date2]])
            ->andFilterWhere(['and',
                ['operationSystem' => $this->operationSystem],
                ['architecture' => $this->architecture],
            ])->groupBy(['DATE_FORMAT(date, "%m-%d-%Y")','browser'])
            ->orderBy('normalizeDate,count desc');

        $query11 = (new Query())
            ->select(['MAX(`t1`.`count`) as `max`','`t1`.`normalizeDate` as `normalizeDate`'])
            ->from(['t1' => $query1])
            ->groupBy('`t1`.`normalizeDate`');

        $query2 = (new Query())
            ->select(['DATE_FORMAT(date, "%m-%d-%Y") as `normalizeDate`', 'count(id) as `count`', 'url'])
            ->from($table)
            ->where(['and',['>=','date', $date1],['<=','date', $date2]])
            ->andFilterWhere(['and',
                ['operationSystem' => $this->operationSystem],
                ['architecture' => $this->architecture],
            ])->groupBy(['DATE_FORMAT(date, "%m-%d-%Y")','url'])
            ->orderBy('normalizeDate, count desc');

        $query22 = (new Query())
            ->select(['MAX(`t2`.`count`) as `max`','`t2`.`normalizeDate` as `normalizeDate`'])
            ->from(['t2' => $query2])
            ->groupBy('`t2`.`normalizeDate`');

        $query3 = (new Query())
            ->select(['DATE_FORMAT(date, "%m-%d-%Y") as `normalizeDate`', 'count(id) as `count`'])
            ->from($table)
            ->where(['and',['>=','date', $date1],['<=','date', $date2]])
            ->andFilterWhere(['and',
                ['operationSystem' => $this->operationSystem],
                ['architecture' => $this->architecture],
            ])->groupBy(['DATE_FORMAT(date, "%m-%d-%Y")']);

        $queryMain = (new Query())
            ->select(['distinct `maxBrowser`.`normalizeDate`','`url`.`url`','`browser`.`browser`','`maxRequest`.`count` as `count`'])
            ->from($table)
            ->leftJoin(['maxBrowser' => $query11], 'DATE_FORMAT(date, "%m-%d-%Y") = `maxBrowser`.`normalizeDate`')
            ->leftJoin(['maxUrl' => $query22], 'DATE_FORMAT(date, "%m-%d-%Y") = `maxUrl`.`normalizeDate`')
            ->leftJoin(['browser' => $query1], '`browser`.`count` = `maxBrowser`.`max`')
            ->leftJoin(['url' => $query2], '`url`.`count` = `maxUrl`.`max`')
            ->leftJoin(['maxRequest' => $query3], '`maxRequest`.`normalizeDate` = DATE_FORMAT(date, "%m-%d-%Y")')
            ->where('`maxBrowser`.`normalizeDate` is NOT NULL');

        return $dataProvider3 = new ArrayDataProvider([
            'allModels' => $queryMain->all(),
            'sort' => [
                'attributes' => [
                    'normalizeDate' => [],
                    'url' => [],
                    'browser' => [],
                    'count' => [],
                ],

                'defaultOrder' => [
                    'normalizeDate' => SORT_ASC
                ]
            ],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
    }

    /**
     * @return array
     */
    public function getOperationSystems()
    {
        $date1 = $this->normalizeDate($this->getDateFrom());
        $date2 = $this->normalizeDate($this->getDateTo());

        return RequestSearch::find()->select('operationSystem')
            ->where(['and',['>=','date', $date1],['<=','date', $date2]])
            ->distinct()->indexBy('operationSystem')->column();
    }

    /**
     * @return array
     */
    public function getArchitecture()
    {
        $date1 = $this->normalizeDate($this->getDateFrom());
        $date2 = $this->normalizeDate($this->getDateTo());

        return RequestSearch::find()->select('architecture')
            ->where(['and',['>=','date', $date1],['<=','date', $date2]])
            ->distinct()->indexBy('architecture')->column();
    }

    /**
     * @return false|string
     */
    public function getDateFrom()
    {
        $date = $this->_dateFrom;
        $stamp = strtotime($date);

        return $this->_dateFrom ?
              date('d-m-Y H:i:s', mktime(0,0,0,date('m',$stamp),date('d',$stamp),date('Y',$stamp)))
            : date('d-m-Y H:i:s', mktime(0,0,0));
    }

    /**
     * @param $value
     */
    public function setDateFrom($value)
    {
        if ($value) {
            $this->_dateFrom = $value;
        }
    }

    /**
     * @return false|string
     */
    public function getDateTo()
    {
        $date = $this->_dateTo;
        $stamp = strtotime($date);

        return $this->_dateTo ?
              date('d-m-Y H:i:s', mktime(23,59,59,date('m',$stamp),date('d',$stamp),date('Y',$stamp)))
            : date('d-m-Y H:i:s', mktime(23,59,59));
    }

    /**
     * @param $value
     */
    public function setDateTo($value)
    {
        if ($value) {
            $this->_dateTo = $value;
        }
    }

    /**
     * @param $date
     * @return string
     */
    private function normalizeDate($date)
    {
        try {
            return (new DateTime($date))->format('Y-m-d H:i:s');
        }catch (Exception $exception) {
            throw new DomainException('Неверный формат даты при поиске статистики');
        }
    }

}