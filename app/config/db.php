<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=' . $_ENV['dbHost'] . ';dbname=' . $_ENV['dbName'],
    'username' => $_ENV['dbUserName'],
    'password' => $_ENV['dbPassword'],
    'charset'  => $_ENV['dbCharset'],
    'attributes' => [
        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET SESSION sql_mode = (SELECT REPLACE(@@sql_mode,"ONLY_FULL_GROUP_BY",""));'
    ]


    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];
